
import os
import parser as vm_parser

if __name__ == '__main__':
    fd_name = 'input_grammar.txt'
    b = True
    if os.path.exists(fd_name):
        fd = open(fd_name, 'r')
        num_line = 0
        stack = []
        while True:
            line = fd.readline()
            num_line = num_line + 1
            if line == '':
                if b and len(stack) == 0:
                    print "\n ---- Parsing correcto ----"
                else:
                    print "\n Stop --> Line[" + str(num_line - 1) + "] "
                break

            b = vm_parser.parsing(line, num_line, stack)
            if b == False:
                break
        fd.close()
    else:
        print "Fichero: " + fd_name + " no existe"