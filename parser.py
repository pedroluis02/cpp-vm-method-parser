
(TYPE_FUNCTION, NAME_FUNCTION) = ('void', 'main')

(PAREN_BEGIN, PAREN_END) = ('(', ')')

(BRACK_BEGIN, BRACK_END) = ('{', '}')
(COM, SEMICOLON) = (',', ';')
(OP_ASSIG) = ('=')

LIST_SE = [PAREN_BEGIN, PAREN_END, BRACK_BEGIN, BRACK_END,
           OP_ASSIG, COM, SEMICOLON]

DOC = {TYPE_FUNCTION :'Tipo void', NAME_FUNCTION :'Funcion principal',
       'int' :'Tipo entero', PAREN_BEGIN :'Parentesis inicio',
       PAREN_END :'Parentesis final' ,BRACK_BEGIN :'Corchete inicio',
       BRACK_END :'Corchete final', COM :'Coma', SEMICOLON :'Punto y coma',
       '+' :'Operador mas' ,'-' :'Operador menos' ,'*' :'Operador mul',
       '*' :'Operador div', OP_ASSIG :'Operador de igualdad'}

DIGIT = tuple([str(i) for i in range(10)]) # <digit> ::= 0 | ... | 9

OPERATOR_B = ('+', '-', '*', '/') # <operator> := '+' ...

TYPE_DATA = ('int', 'float') # <type_data> ::=  'int'

def printDoc(x):
    if x in DOC:
        print x + ": " + DOC[x]
    else:
        print x + ": no definido"

def isLetter(x):
    return ('a' <= x <= 'z') or ('A' <= x <= 'Z')

def isDigit(x):
    return x in DIGIT

def isOperator(x):
    return x in OPERATOR_B

def isTypeData(x):
    return x in TYPE_DATA

def separatorStr(x):
    bp = []
    aux = ''
    i = 0
    while i < len(x):
        if x[i] in LIST_SE or isOperator(x[i]):
            if len(aux) > 0:
                bp.append(aux)
                aux = ''
            bp.append(x[i])
        elif x[i] == ' ' or x[i] == '\n' or x[i] == '\t':
            if len(aux) > 0:
                bp.append(aux)
                aux = ''
            i = i + 1
            continue
        else:
            aux = aux + x[i]
        i = i + 1
        if i == len(x) and len(aux) > 0:
            bp.append(aux)
    del aux, i
    return bp

def isInt(x): # <int> := <digit> | <digit> <int>
    if len(x) == 0:
        return False

    for i in range(len(x)):
        if isDigit(x[i]):
            continue
        else:
            return False

    return True

def isIdentifier(x): # <id>   ::= '_' <id_a> | <letter> <id_a>
    # <id_a> ::= '_' | letter | <digit>
    if len(x) == 0:
        return False
    elif not isLetter(x[0]):
        return False

    for i in range(1, len(x)):
        if isLetter(x[i]) or isDigit(x[i]):
            continue
        else:
            return False
    return True

def isDeclaration(ld, i, nl): # <dec> ::= <type_data> <dec_a> ;
    # <dec_a> ::= <id> |, <id> <dec_a>
    stack_id = []
    tda = '' # tipo de dato actual

    while i < len(ld):
        if isTypeData(ld[i]) and stack_id == []:
            stack_id.append('T')
            tda = ld[i]
            printDoc(ld[i])
        elif i < len(ld) and not isTypeData(ld[i]) and stack_id == []:
            return i
        else:
            if isIdentifier(ld[i]):
                if stack_id[len(stack_id ) -1] == 'T':
                    if not ld[i] in DOC.keys():
                        DOC[ld[i]] = 'Identificador ' + tda
                        printDoc(ld[i])
                        stack_id.append('I')
                    else:
                        print "Stop --> Line[ "+ str(nl) +"] " + ld[i] + ": ya esta definido"
                        return -1
                elif stack_id[len(stack_id) - 1] == 'C':
                    if not ld[i] in DOC.keys():
                        DOC[ld[i]] = 'Identificador ' + tda
                        stack_id.pop()
                        printDoc(ld[i])
                    else:
                        print "Stop --> Line[" + str(nl) + "] " + ld[i] + ": ya esta definido"
                        return -1
                else:
                    print "Stop --> Line[" + str(nl) + "] " + ld[i] + ": no es identificador"
                    return -1
            elif ld[i] == ',' and stack_id[len(stack_id) - 1] == 'I':
                stack_id.append('C')
                printDoc(ld[i])
            elif ld[i] == ';' and stack_id[len(stack_id) - 1] == 'I':
                stack_id.pop()
                stack_id.pop()
                printDoc(ld[i])
            else:
                print "Error --> Line[" + str(nl) + "] :" + ld[i]
                return -1
        i = i + 1

    if len(stack_id) == 0:
        del stack_id, tda
        return i
    else:
        del stack_id, tda
        return -1


def isAssing(ls, i, nl):  # <assig> ::= <id> = <expr>
    # <expr> ::= <expr> <operator> <expr>
    #	        | <id> | <const_int>
    stack_ass = []  #
    while i < len(ls):
        if isIdentifier(ls[i]) and stack_ass == []:
            if ls[i] in DOC:
                stack_ass.append('I')
                printDoc(ls[i])
            else:
                print "Stop --> Line[" + str(nl) + "] " + ls[i] + ": no definido"
                return -1
        elif i < len(ls) and not isIdentifier(ls[i]) and stack_ass == []:
            return i
        else:
            if stack_ass[len(stack_ass) - 1] == 'I' and ls[i] == OP_ASSIG:
                stack_ass.append('A')
                printDoc(ls[i])
            elif stack_ass[len(stack_ass) - 1] == 'A':
                if isIdentifier(ls[i]):
                    if ls[i] in DOC:
                        stack_ass.append('N')
                        printDoc(ls[i])
                    else:
                        print "Stop --> Line[" + str(nl) + "] " + ls[i] + ": no definido"
                        return -1
                elif isInt(ls[i]):
                    stack_ass.append('N')
                    print ls[i] + ": const Int"
            elif stack_ass[len(stack_ass) - 1] == 'N' and isOperator(ls[i]):
                stack_ass.append('O')
                printDoc(ls[i])
            elif stack_ass[len(stack_ass) - 1] == 'O':
                if isIdentifier(ls[i]):
                    if ls[i] in DOC:
                        stack_ass.pop()
                        printDoc(ls[i])
                    else:
                        print "Stop --> Line[" + str(nl) + "] " + ls[i] + ": no definido"
                        return -1
                elif isInt(ls[i]):
                    stack_ass.pop()
                    print ls[i] + ": const Int"
            elif ls[i] == ';' and stack_ass[len(stack_ass) - 1] == 'N':
                stack_ass.pop()
                stack_ass.pop()
                stack_ass.pop()
            else:
                print "Error --> Line[" + str(nl) + "] :" + ls[i]
                return -1
        i = i + 1

    if len(stack_ass) == 0:
        del stack_ass
        return i
    else:
        del stack_ass
        return -1


def parsing(line_str, num_line, stack):
    ltk = separatorStr(line_str)
    i = 0

    while i < len(ltk):
        if stack == []:
            if ltk[i] == TYPE_FUNCTION:
                stack.append('V')
                printDoc(ltk[i])
            else:
                print "Stop --> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False
        elif stack[len(stack) - 1] == 'V' and ltk[i] == NAME_FUNCTION:
            stack.append('M')
            printDoc(ltk[i])

            i = i + 1
            if i == len(ltk):
                print "Stop -> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False

            if stack[len(stack) - 1] == 'M' and ltk[i] == PAREN_BEGIN:
                stack.append('PI')
                printDoc(ltk[i])
            else:
                print "Stop --> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False

            i = i + 1
            if i == len(ltk):
                print "stop -> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False

            if stack[len(stack) - 1] == 'PI' and ltk[i] == PAREN_END:
                stack.append('PF')
                printDoc(ltk[i])
            else:
                print "Stop --> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False

        elif stack[len(stack) - 1] == 'PF' and ltk[i] == BRACK_BEGIN:
            stack.append('BI')
            printDoc(ltk[i])
        elif stack[len(stack) - 1] == 'BI' and isTypeData(ltk[i]):
            i = isDeclaration(ltk, i, num_line)
            if i == -1:
                print "Stop --> Line[%d]" % num_line
                return False
            else:
                continue
        elif stack[len(stack) - 1] == 'BI' and isIdentifier(ltk[i]):
            i = isAssing(ltk, i, num_line)
            if i == -1:
                print "Stop --> Line[%d]" % num_line
                return False
            else:
                continue
        elif (stack[len(stack) - 1] == 'BI') and ltk[i] == BRACK_END:
            if len(stack) >= 5:
                for k in range(5):
                    stack.pop()
                printDoc(ltk[i])
                return True
            else:
                print "Stop --> Line[" + str(num_line) + "] ", printDoc(ltk[i])
                return False
        else:
            print "Stop --> Line[" + str(num_line) + "]", printDoc(ltk[i])
            return False
        i = i + 1

    return True
